FROM java:8
ADD target/contentservice.jar contentservice.jar
ENTRYPOINT ["java","-jar","contentservice.jar"]
