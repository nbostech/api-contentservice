package com.wavelabs.cs.util;

import java.io.StringWriter;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public class ImageUriParser {
	static final Logger log = Logger.getLogger(ImageUriParser.class);
	@Autowired
	static Environment env;

	private ImageUriParser() {
	}

	@SuppressWarnings("unchecked")
	public static String getJsonString(String jStr) {

		JSONParser parser = new JSONParser();
		JSONObject obj;
		JSONArray items;
		try {
			obj = (JSONObject) parser.parse(jStr);
			System.out.println(obj);
			items = (JSONArray) obj.get("items");
			for (int i = 0; i < items.size(); i++) {
				JSONObject obj2 = (JSONObject) items.get(i);
				System.out.println(obj2);
				JSONObject image = parseImageUri((JSONObject) obj2.get("image"));
				System.out.println("image -" + image);
				obj2.put("image", image);
				items.set(i, obj2);
			}
			obj.put("items", items);
			StringWriter out = new StringWriter();
			obj.writeJSONString(out);
			System.out.println(obj);
			jStr = out.toString();
			System.out.println(jStr);

		} catch (Exception e) {
			log.error(e);
		}

		return jStr;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject parseImageUri(JSONObject image) throws UnknownHostException {

		String path = (String) image.get("path");
		System.out.println(path);
		//String host = env.getProperty("host");
		//String port = env.getProperty("server.port");
		//System.out.println("port-" + port);
		//path = "http://"+ host + ":" + port +"/site/binaries" + path;
		path = "http://cms.50k.nbos.io/site/binaries" + path;
		System.out.println("path-" + path);
		image.put("path", path);
		return image;
	}

	@SuppressWarnings("unchecked")
	public static String getJsonString1(String jStr) {

		JSONParser parser = new JSONParser();
		JSONObject obj;
		JSONArray items;
		try {
			obj = (JSONObject) parser.parse(jStr);
			System.out.println(obj);
			items = (JSONArray) obj.get("items");
			for (int i = 0; i < items.size(); i++) {
				JSONObject obj2 = (JSONObject) items.get(i);
				System.out.println(obj2);
				JSONObject image = parseImageUri((JSONObject) obj2.get("logo"));
				System.out.println("image -" + image);
				obj2.put("image", image);
				items.set(i, obj2);
			}
			obj.put("items", items);
			StringWriter out = new StringWriter();
			obj.writeJSONString(out);
			System.out.println(obj);
			jStr = out.toString();
			System.out.println(jStr);

		} catch (Exception e) {
			log.error(e);
		}

		return jStr;
	}

	
	
}
