package com.wavelabs.cs.controller;


import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.wavelabs.cs.util.ImageUriParser;

import io.nbos.capi.api.v0.models.RestMessage;

@Controller
@RequestMapping("/deal")
public class DealsController {
	static final Logger log = Logger.getLogger(DealsController.class);
	@Autowired
	Environment env;

	@CrossOrigin
	@RequestMapping(value = "/all")
	public @ResponseBody String getDeals() {
		RestTemplate restTemplate = new RestTemplate();
		String uri = env.getProperty("dealUrl");
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		return ImageUriParser.getJsonString1(response.getBody());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@CrossOrigin
	@RequestMapping("/{dealId}")
	public @ResponseBody ResponseEntity getDealById(@PathVariable String dealId) {
		RestTemplate restTemplate = new RestTemplate();
		String uri = env.getProperty("dealUrl");
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		RestMessage restMessage = new RestMessage();

		String jsonStr = response.getBody();
		System.out.println("response.getBody-"+jsonStr);
		JSONParser parser = new JSONParser();
		JSONObject obj;
		JSONArray items;
		try {
			obj = (JSONObject) parser.parse(jsonStr);
			System.out.println("obj2 -"+obj);
			items = (JSONArray) obj.get("items");
			for (int i = 0; i < items.size(); i++) {
				JSONObject obj2 = (JSONObject) items.get(i);
				System.out.println("get items-"+obj2);
				String id = (String) obj2.get("id");
				if (id.equals(dealId)) {
					JSONObject image = ImageUriParser.parseImageUri((JSONObject) obj2.get("logo"));
					obj2.put("logo", image);
					items.set(i, obj2);
					return ResponseEntity.status(200).body(obj2);
				}

			}

		} catch (Exception e) {
			log.error(e);
		}
		restMessage.message = "Deal not found";
		restMessage.messageCode = "404";
		return ResponseEntity.status(404).body(restMessage);

	}
	@SuppressWarnings({ "rawtypes", "unused" })
	@CrossOrigin
	@RequestMapping("/dealName/{dealName}")
	public @ResponseBody ResponseEntity getDealByName(@PathVariable String dealName) {
		RestTemplate restTemplate = new RestTemplate();
		String uri = env.getProperty("dealUrl");
		ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
		RestMessage restMessage = new RestMessage();

		String jsonStr = response.getBody();
		System.out.println("response.getBody-"+jsonStr);
		JSONParser parser = new JSONParser();
		JSONObject obj;
		JSONArray items;
		try {
			obj = (JSONObject) parser.parse(jsonStr);
			System.out.println("obj2 -"+obj);
			items = (JSONArray) obj.get("items");
			for (int i = 0; i < items.size(); i++) {
				JSONObject obj2 = (JSONObject) items.get(i);
				String name = (String) obj2.get("company");
			/*	if (name.equals(dealName)) {
					JSONObject image = ImageUriParser.parseImageUri((JSONObject) obj2.get("image"));
					obj2.put("image", image);
					items.set(i, obj2);*/
					return ResponseEntity.status(200).body(name);
				}

			

		} catch (Exception e) {
			log.error(e);
		}
		restMessage.message = "Deal name not found";
		restMessage.messageCode = "404";
		return ResponseEntity.status(404).body(restMessage);

	}
}
